Created 180414

OpenGrok configration files

tomcat root page location: */usr/local/apache-tomcat-8.0/webapps/ROOT*

OpenGrok default logo: */usr/local/apache-tomcat-8.0/webapps/source/default/img/icon.png*

NOTE: those files owned by www:www

patches

    server.xml.patch:   /usr/local/apache-tomcat-8.0/conf

    OpenGrok.patch:     /var/opengrok/bin

    web.xml.patch:      /var/opengrok/web/source/WEB-INF

    index.jsp.patch:    /usr/local/apache-tomcat-8.0/webapps/source

Index

    /var/opengrok/bin/OpenGrok index

